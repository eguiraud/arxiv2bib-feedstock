# arxiv2bib-feedstock<br>
[![install with conda](https://anaconda.org/bluehood/arxiv2bib/badges/installer/conda.svg)](https://anaconda.org/bluehood/arxiv2bib)
[![last_built](https://anaconda.org/bluehood/arxiv2bib/badges/latest_release_date.svg)](https://anaconda.org/bluehood/arxiv2bib)
[![license](https://anaconda.org/bluehood/arxiv2bib/badges/license.svg)](https://anaconda.org/bluehood/arxiv2bib)

I am not related to arxiv2bib's development in any way, I just created a conda package for it.
Refer to the [project homepage](http://nathangrigg.github.io/arxiv2bib) for questions or bug reports.

## Conda installation
```bash
$ conda install -c bluehood arxiv2bib
```
